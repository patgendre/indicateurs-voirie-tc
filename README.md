# Open Mobility Indicators

Open Mobility Indicators is a set of free and collaborative software tools that process open data to allow the creation of applications that make visible the quality of accessibility and local mobility of a neighborhood, a city or any territory.
**THE REPO HAS MOVED** TO [https://gitlab.com/open-mobility-indicators/](https://gitlab.com/open-mobility-indicators/)

## License

[EUPL v1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12): akin to an AGPL, but more readable and translated and legally binding into all languages of the EU.
