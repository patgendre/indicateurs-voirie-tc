# 0. création de la base de données   
## 0A) installation de la base postgresql   
Postgresql + extension postgis déjà installée, avec pgadmin4    
`sudo service postgresql start`   

```
CREATE DATABASE osm
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'fr_FR.UTF-8'
    LC_CTYPE = 'fr_FR.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
CREATE EXTENSION postgis
    SCHEMA public
    VERSION '2.5.1';
create extension hstore;
```

## 0B) récupération des données OSM   
Sur territoire test : commune d'Aix-en-Provence   
Téléchargements de PACA en PBF `http://download.geofabrik.de/europe/france.html`   
Récup du contour de la commune aix-en-provence `https://www.openstreetmap.org/relation/70279`    
en le téléchargeant au format Poly `http://polygons.openstreetmap.fr/?id=70279`   
 
Install de osmconvert : `https://wiki.openstreetmap.org/wiki/Osmconvert`   
`apt install osmctools`    
`osmconvert provence-alpes-cote-d-azur-latest.osm.pbf -B=aix.poly -o=aix.pbf`    
Le pbf d'aix fait 5Mo contre 270 pour celui de Paca   

# 1. import des données OSM de voirie dans la base   

Trois approches possibles : avec le modèle Osmosis, avec la lib OSM4 routing, en réutilisant sprawlmap.

## 1A) avec OSMOSIS

cf. [la doc précédente](https://gitlab.com/cerema-mobilite/osm_voirie/wikis/2.2.-importer-les-donnees-dans-postgresql):  
Installation d'OSMOSIS.  
Création des 2 schémas et `PGSNAPSHOT_SCHEMA` et `PGSNAPSHOT_SCHEMA LINESTRING`  

`osmosis/bin/osmosis --read-pbf file='aix.pbf' --write-pgsql host=localhost database='osm' user='postgres' password='lemotdepasse'`   

La base contient maintenant les tables du schéma OSMOSIS : nodes, way, relations, way_nodes, etc.   

### création d'une table voirie

```
CREATE TABLE public.voirie
(
id bigint,
longueur text,
vehicules text,
velos text,
pietons text,
user_id integer NOT NULL,
tstamp timestamp without time zone NOT NULL,
tags hstore,
linestring geometry(geometry,4326),
CONSTRAINT pk_voirie PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE public.voirie
OWNER TO postgres;
```

Pour distinguer la voirie empruntable en voiture, à vélo, à pied, il est essentiel de bien définir les règles de [sélection des données OSM par tag](https://gitlab.com/cerema-mobilite/osm_voirie/wikis/2.3.-realiser-des-requetes-sur-les-differents-tags).   

Peuplement de la table voirie par une requête SQL [cf. la doc précédente où la table voirie s'appelle GLOBAL](https://gitlab.com/cerema-mobilite/osm_voirie/blob/Scripts/Requete_SQL_Global)  
La table contient des colonnes "`vehicules`", "`pietons`", "`velos`" contenant un tag décrivant le type de voie, permettant de savoir quels sections conserver dans le réseau marche, vélo ou voiture, et éventuellement de la qualifier (pour des indicateurs qualitatifs sur la voirie qui pourraient être produits dans un 2ème temps).
 
En fait cette requête d'un stage antérieur est à corriger; par exemple: dans la requête,  la valeur 'vehicules non autorises' dans la colonne vehicules correspond à toutes les lines (bâtiment, frontière admin), il faut donc les supprimer de la table. Il y a une autre valeur 'véhicules interdits' pour par ex. les sentiers et les pistes cyclables.  
Autre exemple: j'ai trouvé un highway=steps (escalier) qui a été classé dans 'vehicule non autorise' et donc supprimé.  
`delete from public.voirie where vehicules='vehicules non autorises';` 
`DELETE from voirie where vehicules='acces prive' or pietons='acces prive' or velos='acces prive';`     
Pour le PoC il faudra revoir les critères de sélection et extraire proprement le réseau marchable (pour le vélo et la voiture ça pourra être fait dans un 2ème temps).

### sectionner les tronçons de voirie

Pour créer les mailles (ilots / pâtés de maison) à partir du linéaire de voirie, il faut une topologie propre en redécoupant les tronçons de voirie pour être sûr qu'ils ne se chevauchent pas (sauf si cela correspond à un passage inférieur - souterrain -, ou supérieur - pont -), et donc que tous les segments se terminent bien à une intersection (ou dans une impasse).   

On commence par créer une table des intersections entre tronçons de voirie OSM
```
CREATE TABLE intersections as
SELECT
ST_Intersection(r.linestring, s.linestring) AS inter_geom,
r.id as GID1,
s.id as GID2
FROM
voirie AS r,
voirie AS s
WHERE ST_Intersects(r.linestring, s.linestring)
AND (r.id < s.id);
-- le < permet que les points d'intersection n'apparaissent qu'une fois ; revient à boucler sur la moitié du tableau gid1 x xgid2

-- attention pour que gid fournisse une valeur unique de clé il faut qu'il y ait moins de 10000 lignes (sinon ajouter un zéro!)

ALTER TABLE intersections ADD column gid bigint;
UPDATE intersections SET gid = 10000*gid1 + gid2;
-- en fait on a deux fois les intersections car on boucle sur tous les tronçons
-- donc la table inter contient chaque point 2 fois
-- et la commande suivante ne garde qu'un seul point 

-- dans cette table on ne garde que les points
-- les autres intersections peuvent etre :
--      des linestring (si chevauchement de tronçons, qu'il faudra éliminer par ailleurs!!)
--      des multipoint (par exemple deux tronçons qui forme une boucle ont 2 points d'intersection)
-- ces cas seront peut-être à reconsidérer, à court terme on les supprime

DELETE FROM intersections
WHERE GeometryType(inter_geom) != 'POINT' ;
```

On crée une nouvelle tables voie à partir de voirie avec une topologie utilisable pour créer les mailles.   
Pour cela on utilise fonction PL/SQL [split_lines2](https://gitlab.com/patgendre/indicateurs-voirie-tc/blob/master/__split_lines2.SQL)     

```
CREATE TABLE voies AS select * FROM __split_lines2('select id as id, linestring as geom from voirie','select inter_geom as geom, gid as pid from intersections');
DELETE FROM voies WHERE GeometryType(line) != 'LINESTRING' ;

ALTER TABLE voies ADD COLUMN vehicules text;
ALTER TABLE voies ADD COLUMN velos text;
ALTER TABLE voies ADD COLUMN pietons text;
ALTER TABLE voies ADD COLUMN tags hstore;
	
ALTER TABLE voies 
RENAME COLUMN lineid TO id;
UPDATE voies
SET tags=voirie.tags,
vehicules=voirie.vehicules,
pietons=voirie.pietons,
velos=voirie.velos
FROM voirie WHERE voirie.id=voies.id ;
```



## 1B) avec OSM4ROUTING

[Osm4routing2](https://github.com/Tristramg/osm4routing2) est une librairie développée par Tristram Gräbener; la nouvelle version en Rust s'exécute beaucoup plus vite que notre traitement SQL. C'est donc cette solution qu'il faut retenir au lieu de l'import des données pdf dans le modèle OSMOSIS, car nous n'avons besoin que des données ways d'OSM.   
Néanmoins si on veut modifier les tags permettant de sélectionner les tronçons du réseau de voirie (piéton, cyclable), il faut [modifier à la marge le code rust](https://github.com/Tristramg/osm4routing2/blob/master/src/osm4routing/categorize.rs) et le recompiler.   

Attention pour faire fonctionner osm4routing, il faut que le fichier pbf soit découpé proprement (avec tous les noeuds des ways). Il y a une option pour cela lorsqu'on éxtrait les données avec osmconvert   
`osmconvert --complete-ways provence-alpes-cote-d-azur-latest.osm.pbf -B=aix.poly -o=aix2.pbf`

La commande osm4routing aix2.pbf crée ensuite les fichiers edges.csv, nodes.csv très rapidement.
Il reste ensuite à importer le fichier edges.csv produit par osm4routing dans postgis.

```
CREATE TABLE edges
(
  id bigint,
  source bigint,
  target bigint,
  length double precision,
  foot smallint,
  car_forward smallint,
  car_backward smallint,
  bike_forward smallint,
  bike_backward smallint,
  wkt varchar
);

ALTER TABLE edges ADD COLUMN the_geom geometry;
UPDATE edges SET the_geom = ST_GeomFromText(wkt, 4326);
ALTER TABLE edges ADD CONSTRAINT enforce_dims_the_geom CHECK (st_ndims(the_geom) = 2);
ALTER TABLE edges ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(the_geom) = 'LINESTRING'::text OR the_geom IS NULL);
ALTER TABLE edges ADD CONSTRAINT enforce_srid_the_geom CHECK (st_srid(the_geom) = 4326);

CREATE INDEX the_geom_gist
  ON edges
  USING gist
  (the_geom );

CREATE TABLE blocks AS SELECT (ST_Dump(ST_Polygonize(the_geom))).geom FROM edges WHERE foot=1;

CREATE TABLE blocklines AS SELECT (ST_ExteriorRing(ST_makevalid(geom))) as geom FROM blocks;
```


Le réseau viaire produit est globalement correct, il reste à améliorer certains points:    
- certaines mailles sont des iles (reliées au reste du réseau par une impasse) : faut-il les supprimer? par exemple en supprimant les mailles incluses dans un autre?   
- dans osm4routing, les edges ne comprennent pas les escaliers ni les trottoirs : il faudrait les ajouter pour la marche (en modifiant et recompilant le code rust);   
- nous avons eu certaines erreurs de topologie, selon les données en entrée, exemple:  
```
 ERROR:  GEOSContains: TopologyException: side location conflict at 3.8825552475954832 43.613169457455193
SQL state: XX000
```
    
### détection des impasses   

```
ALTER TABLE edges ADD COLUMN deadend BOOLEAN;
ALTER TABLE edges ADD COLUMN uid SERIAL PRIMARY KEY;
SELECT edges.the_geom FROM edges,blocks WHERE ST_within(edges.the_geom,blocks.geom);
```   

Le reste des traitements de calcul d'indicateurs est similaire que l'on utilise Osmosis ou Osm4routing pour importer les données (§2 suivant).   

### 1C) avec SPRAWLMAP

SprawlMap est un projet de l'université Mc Gill à Montréal, ciblé sur un indicateur de perméabilité du réseau viaire, calculé sur le monde entier à partir des données OSM et [publié sur une carte interactive](https://sprawlmap.org/).   
Les données et le code sont publiés sur https://gitlab.com/cpbl/global-sprawl-2020 et réutilisables. Seule la partie initiale de pré-traitement des données OSM afin d'en extraire le réseau viaire en distiguant impasses, boucles et tronçons faisant partie d'une maille du réseau, pas forcément le calcul de l'indicateur SNDI, néanmoins intéressant à terme.   
Néanmoins les données ne correspondent pas exactement à celles que nous souhaitons car il faudrait refaire les traitements sur le réseau piéton ou cyclable. Nous n'avons pas testé cette 3ème solution pour l'instant, mais ça semble prometteur.

# 2. créer la donnée décrivant la perméabilité du réseau     
## 2A) créer les mailles du réseau
En principe les composantes connexes seront des îles, mais il y aussi des ronds-points et autres tous petits ilots (parkings privés, etc.) qu'il faudrait supprimer (par exemple si le périmètre est inférieur à 10 mètres).  
Utilisation de la fonction postgis `ST_polygonize`:   
`CREATE TABLE mailles AS SELECT (ST_Dump(ST_Polygonize(line))).geom FROM voies;`

ou mieux   
`CREATE TABLE mailles AS SELECT (ST_Dump(ST_Polygonize(line))).geom FROM voies WHERE pietons<>'interdit aux pietons';`
    
## 2B) calcul des impasses  
calcul des impasses: ce sont les voies qui n'appartiennent pas à une maille.

```
ALTER TABLE voies 
ADD COLUMN deadend BOOLEAN;

CREATE TABLE maillelines AS SELECT ST_LINEMERGE(ST_UNION(ST_ExteriorRing(ST_makevalid(geom)))) as geometry FROM mailles;

CREATE INDEX geom_idx
  ON maillelines
  USING GIST (geometry);
CREATE INDEX voies_geom_idx
  ON voies
  USING GIST (line);
CREATE INDEX mailles_geom_idx
  ON mailles
  USING GIST (geom);

UPDATE voies SET deadend=false WHERE id IS NOT null;
-- false par défaut
```

1er test fait avec une fonction [__detect_deadends](https://gitlab.com/patgendre/indicateurs-voirie-tc/blob/master/__detect_deadends), qui regarde si les tronçons appartiennent aux mailles, sinon deadend est true : `deadendflag = NOT(ST_within(rec_voie.line,maillelines.geometry)) from maillelines;`     
Pour l'exécuter, simplement `SELECT __detect_deadends();`.   

![Impasses et Mailles à Aix|small](uploads/212aadc3a3386d6bdc95e0df7f48c5e5/impasses-aix.png "Impasses et Mailles à Aix")    

Le temps de calcul est de l'ordre d'1H30 pour Aix-en-Provence, sans doute à optimiser.
Attention il y a des entrées de la table voies sans géométrie (233).
Egalement, il faudrait refaire le calcul avec uniquement les voies autorisées aux piétons (suppressions des autoroutes).   
Puis étiqueter les voies faisant partie d'une maille / d'une impasse, ce pour les 3 sous-réseaux considétés (voiture, vélo, marche)   

# 3. indicateurs sur les mailles
Cf. le rapport stage de 2014, on pourrait aussi calculer beaucoup d'indicateurs de base sur la voirie (km de voies ou d'impasses / habitant / km2 ou par commune etc.)  

```
ALTER TABLE mailles 
ADD COLUMN pop REAL;
ALTER TABLE mailles 
ADD COLUMN perim REAL;
ALTER TABLE mailles 
ADD COLUMN surf REAL;
ALTER TABLE mailles
ADD COLUMN gid SERIAL;
```


## périmètre et surface  
`UPDATE 'mailles' SET perim=ST_Perimeter(geom::geography), surf=ST_Area(geom::geography);`
-- attention aux unités : en mètres, m2

## population et densité  

Fonction PLSQL : [__pop_par_maille](https://gitlab.com/patgendre/indicateurs-voirie-tc/blob/master/__pop_par_maille.SQL) .
`SELECT __pop_par_maille();`
-- calculé en 15 sec environ pour Aix
```
ALTER TABLE mailles ADD COLUMN densite REAL;
UPDATE 'mailles' SET densite=pop*1000000.0/surf;
```

## emplois   
[Création d'une table geosiret](https://gitlab.com/patgendre/indicateurs-voirie-tc/wikis/Donn%C3%A9es-d'Emploi) permettant d'avoir le nb d'emplois géoréférencés (mais très approximatif).    

## score de mobilité

On propose un mix entre population (et emplois) vs taille de l'ilot.   
En rouge les grands ilots avec des habitants, les blanc les zones vides, en vert les petits ilots avec des habitants (ou des emplois).   

Trois seuils de densité hab/km2 et 2 seuils de périmètre (350 et 700 mètres, soit 5 et 10 minutes resp. pour en faire le tour):  
- densités seuils: D0<100, D1<1000, D2<10000, D3>=10000  
- périmètres: 350, 700, et au-delà  

Un 1er jet donne ça:   
![mailles-rouge-vert](uploads/a57fcc283a09e05c768e75aeb11fd398/mailles-rouge-vert.png)   

```
ALTER TABLE mailles 
ADD COLUMN score VARCHAR(20);
UPDATE 'mailles' SET score='blanc'
WHERE densite<100;
UPDATE 'mailles' SET score='vertclair'
WHERE densite>100 AND densite<1000 AND perim<350;
UPDATE 'mailles' SET score='jauneclair'
WHERE densite>100 AND densite<1000 AND perim>=350 AND perim<700;
UPDATE 'mailles' SET score='rougeclair'
WHERE densite>100 AND densite<1000 AND perim>=700;
UPDATE 'mailles' SET score='vertmoyen'
WHERE densite>1000 AND densite<10000 AND perim<350;
UPDATE 'mailles' SET score='jaunemoyen'
WHERE densite>1000 AND densite<10000 AND perim>=350 AND perim<700;
UPDATE 'mailles' SET score='rougemoyen'
WHERE densite>1000 AND densite<10000 AND perim>=700;
UPDATE 'mailles' SET score='vertfort'
WHERE densite>=10000 AND perim<350;
UPDATE 'mailles' SET score='jaunefort'
WHERE densite>=10000 AND perim>=350 AND perim<700;
UPDATE 'mailles' SET score='rougefort'
WHERE densite>=10000 AND perim>=700;
```

