Les données sont a priori celles des offres théoriques de TC de transport.data.gouv.fr (il y a pour l'instant plus de données dans OSM mais on prend le parti de valoriser transport.data).  

# Données OSM
On commence par récupérer les données d'arrêt de transport public dans OSM, mais clairement les données GTFS permettront de calculer des indicateurs plus riches (fréquence de passage à un arrêt, vitesse moyenne sur un tronçon...).   

Récupération des données d'arrêt par une [requête overpass](https://gitlab.com/patgendre/indicateurs-voirie-tc/blob/master/aixstops-op-query.txt) faite en ligne sur https://overpass-turbo.eu/.   
Le résultat est un fichier aixstops.CSV.   

Création d'un table stops géo-référencés dans notre table OSM   
```
CREATE TABLE stops (
id varchar(15),
lat numeric,
lon numeric,
name varchar(80),
bus boolean,
train boolean
);
COPY stops FROM '//home/patgendre/Documents/test-indic/aixstops.csv' DELIMITERS ',' CSV HEADER;
ALTER TABLE stops ADD COLUMN geom geometry(Point, 4326);
UPDATE stops SET geom = ST_SetSRID(ST_MakePoint(lon, lat), 4326);
ALTER TABLE stops ADD COLUMN pop real;
```
on enlève les arrêts qui ne sont ni train ni bus (sur Aix dans le résultat de la requête overpass, il n'y en avait pas sauf des stop areas, "arrêts logiques"), soit 179 sur 2668.   
`delete from stops where bus is null and train is null;`   
`update stops set train=false where train is null;`   
`update stops set bus=false where bus is null;`   

# Données GTFS

On utilise le format standard pour l'open data GTFS. Une librairie comme [gtfs-lib](https://github.com/afimb/gtfslib-python) peut faciliter le calcul des indicateurs.
Les données open data officielles sont ici : 
Au niveau mondial, il y a : 

## arrêts
On extrait les arrêts. Il faudra distinguer les arrêts "physiques" et les zones d'arrêts (regroupant les arrêts permettant des correspondances entre lignes, et les 2 sens d'une ligne) qui peuvent être plus pertinents pour des représentations carto. Donc en principe 2 couches pour les arrêts, une couche pour commencer.
Les indicateurs associés peuvent être:
- le nombre de passage par jour (éventuellement en prenant le jour où ce nb est le plus élevé, en semaine hors vacances; le Cerema a déjà [travaillé sur ces requêtes](https://mim.cete-aix.fr/spip.php?article347))
- le nombre de passage en Heure de Pointe
- le nombre d'habitants et d'emplois à moins de 350m (5' à pied)
 
## tronçons
On pourrait aussi extraire les tronçons de ligne, ce qui pourra être fait dans un 2ème.
Cela pose le problème 
Les indicateurs associés sont la vitesse moyenne, le nombre de trajets par jour, le nb de trajets en Heure de Pointe


## accessibilité
La donnée d'offre théorique de TC permet potentiellement une grande richesse d'indicateurs autour de l'accessibilité. Des développements opensource ont été réalisés [par le Cerema avec l'agence d'urbanisme de Marseille en 2015](https://mim.cete-aix.fr/spip.php?article360), ils peuvent être réalisés. Sur France entière, cela poserait pas mal de difficultés techniques, mais cela peut être fait dans un 2ème temps et progressivement.