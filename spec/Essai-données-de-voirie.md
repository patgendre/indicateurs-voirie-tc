# Création des données de voirie

# récup des données OSM, install et import dans PG/Osmosis
Pour le portail il faudrait travailler sur France entière, ici je me limite à Aix-en-Provence... téléchargements de PACA en PBF http://download.geofabrik.de/europe/france.html
récup du contour de la commune aix-en-provence https://www.openstreetmap.org/relation/70279
en le téléchargeant au format Poly http://polygons.openstreetmap.fr/?id=70279


```
install osmconvert https://wiki.openstreetmap.org/wiki/Osmconvert
apt install osmctools
osmconvert provence-alpes-cote-d-azur-latest.osm.pbf -B=aix.poly -o=aix.pbf
```
le pbf d'aix fait 5Mo contre 270 pour celui de Paca

Téléchargement de OSMOSIS dans un dossier Documents/test-indic/osmosis
Postgrsql + extension postgis déjà installée, avec pgadmin4 
`sudo service postgresql start`

```
CREATE DATABASE osm
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'fr_FR.UTF-8'
    LC_CTYPE = 'fr_FR.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
CREATE EXTENSION postgis
    SCHEMA public
    VERSION "2.5.1";
create extension hstore;
```

cf. https://gitlab.com/cerema-mobilite/osm_voirie/wikis/2.2.-importer-les-donnees-dans-postgresql
Création des 2 schémas et PGSNAPSHOT_SCHEMA et PGSNAPSHOT_SCHEMA LINESTRING


osmosis/bin/osmosis --read-pbf file="aix.pbf" --write-pgsql host=localhost database="osm" user="postgres" password="lemotdepasse"

# création de la table voirie

```
CREATE TABLE public.voirie
(
id bigint,
longueur text,
vehicules text,
velos text,
pietons text,
user_id integer NOT NULL,
tstamp timestamp without time zone NOT NULL,
tags hstore,
linestring geometry(geometry,4326),
CONSTRAINT pk_voirie PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE public.voirie
OWNER TO postgres;
```

Remplissage de la table voirie (exécution de la [requête GLOBAL dans la doc](https://gitlab.com/cerema-mobilite/osm_voirie/blob/Scripts/Requete_SQL_Global) :  12000 tronçons environ pour la commune d'Aix.  
En fait dans cette requête la valeur "vehicules non autorises" dans la colonne vehicules correspond à toutes les lines (bâtiment, frontière admin), il faut donc les supprimer de la table. Il y a une autre valeur "véhicules interdits" pour par ex. les sentiers et les pistes cyclables.
La requête est à améliorer car par exemple j'ai trouvé un highway=steps (escalier) qui a été classé dans "vehicule non autorise" et donc supprimé...

`delete from public.voirie where vehicules='vehicules non autorises';`

Peu importe... de toutes façons, il faudra revoir dans un 2ème temps en détail les critères de sélection et lister toutes les valeurs uniques présentes dans les 3 colonnes véhicule, vélo, marche.

```
SELECT
   DISTINCT vehicules
FROM
   voirie;
   de même pour velos et pietons
```
On peut déjà supprimer la voirie privée:

`DELETE from voirie where vehicules='acces prive' or pietons='acces prive' or velos='acces prive;`

# création des mailles
En attendant essayons de récréer les mailles et détecter les impasses. (il faudra ajouter une colonne deadend à la table voirie, pour tagger les impasses)

```
CREATE TABLE mailles as SELECT 
     (ST_Dump(ST_Polygonize(linestring)).geom)
   FROM voirie;
```

Problème déjà identifié et résolu en 2009, il faut nettoyer les données avant, les noeuds ne sont pas confondus aux intersections... Même si on ne veut pas vraiment créer un graphe pour faire du calcul d'itinéraire, juste créer des polygones, c'est bien [le problème à résoudre]().
Cf. par exemple [GISstackexchange](https://gis.stackexchange.com/questions/34687/how-to-split-osm-roads-into-individual-segments-at-intersections)

L'idée est de créer de découper (split) les tronçons de voirie lorsqu'ils intersectent un autre tronçons. Evidemment ça va créer des mailles incorrectes lors que 2 tronçons sont en fait un pont / un tunnel, mais au niveau où on en est, on ne résoudra pas ce pb.
On trouve quand même 19000 intersections pour Aix
```
CREATE TABLE intersections as
SELECT
ST_Intersection(r.linestring, s.linestring) AS inter_geom,
r.id as GID1,
s.id as GID2
FROM
voirie AS r,
voirie AS s
WHERE ST_Intersects(r.linestring, s.linestring)
AND (r.id < s.id);
-- le < permet que les points d'intersection n'apparaissent qu'une fois ; revient à boucler sur la moitié du tableau gid1 x xgid2
```

Ajout d'une clé, on supprime les intersections autres que des points (600 objets sur 19000...)
```
ALTER TABLE intersections ADD column gid text;
UPDATE intersections SET gid = ('N'||gid1)||gid2;
ALTER TABLE intersections ADD PRIMARY KEY (gid);
-- dans cette table on ne garde que les points
-- les autres intersections peuvent etre :
--      des linestring (si chevauchement de tronçons, qu'il faudra éliminer par ailleurs!!)
--      des multipoint (par exemple deux tronçons qui forme une boucle ont 2 points d'intersection)
-- ces cas seront peut-être à reconsidérer, à court terme on les supprime

DELETE FROM intersections
WHERE GeometryType(inter_geom) != 'POINT' ;
```
Ajout des données dans la tables des tronçons de voirie redécoupés et renommées VOIES (il y aurait sûrement moyen de faire un JOIN en ligne!).
```
ALTER TABLE seg RENAME TO voies;
ALTER TABLE voies ADD COLUMN vehicules text;
ALTER TABLE voies ADD COLUMN velos text;
ALTER TABLE voies ADD COLUMN pietons text;
ALTER TABLE voies ADD COLUMN tags hstore;
UPDATE voies SET tags=voies.tags FROM voirie WHERE voies.id=voirie.id ;
UPDATE voies SET vehicules=voies.vehicules FROM voirie WHERE voies.id=voirie.id ;
UPDATE voies SET pietons=voies.pietons FROM voirie WHERE voies.id=voirie.id ;
UPDATE voies SET velos=voies.velos FROM voirie WHERE voies.id=voirie.id ;
```