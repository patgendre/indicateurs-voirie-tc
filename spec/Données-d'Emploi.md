De même que pour la population, extraire la donnée de localisation des emplois de la [base SIRENE](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/).


Un travail avait été fait dans le cadre du projet Accessibilité multimodale Marseille pour géocoder les adresses des établissements sur le territoire marseillais, et géocoder ainsi le nombre d'emplois total sur le territoire; le principe pourrait être repris sur France entière.   
On sait bien sûr que l'effectif attaché à un établissement comporte beaucoup de biais.   
Mais le problème existe aussi pour la population globale, par exemple pour les territoires touristiques on sait que la population recensée est loin de correspondre à la population sur le territoire selon la saison.   

Création d'une table geo_siret extraite au départ des données SIRENE publiées sur opendataarchive pour le dépt 13:   
`https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/#_`
`http://data.cquest.org/geo_sirene/v2019/2019-11/dep/`

On ne garde que les colonnes 3,6,49,50 : cut ne marche pas à cause de pb de séparateurs, il faut utiliser un petit script python:
```
import csv
f = open('geosiret13.csv', 'w')
with open('geo_siret_13.csv') as csvfile:
    linereader = csv.reader(csvfile, delimiter=',', quotechar=''')
    for row in linereader:
        f.write(row[2]+','+row[5]+','+row[48]+','+row[49]+'\n')
f.close()
```

Création de la table;
```
CREATE TABLE geosiret (
SIRET varchar(15),
TEFET varchar(6),
lon numeric,
lat numeric
);
COPY geosiret FROM '//home/patgendre/Documents/test-indic/insee/SIRENE/geosiret13.csv' DELIMITERS ',' CSV HEADER;
```

Très approximatif : on mappe les tranches d'effectifs d'établissements sur une valeur de nombre d'emplois.
Sachant aussi qu'on n'a pas les emplois publics du type écoles...

```
ALTER TABLE geosiret 
ADD COLUMN nbemp REAL;
liste des valeurs : SELECT DISTINCT tefet from geosiret;
UPDATE geosiret SET nbemp=0 WHERE tefet='NN' or TEFET='0' or TEFET='00';
UPDATE geosiret SET nbemp=0 WHERE tefet is null;
UPDATE geosiret SET nbemp=1 WHERE tefet='01';
UPDATE geosiret SET nbemp=3 WHERE tefet='02';
UPDATE geosiret SET nbemp=7 WHERE tefet='03';
UPDATE geosiret SET nbemp=12 WHERE tefet='11';
UPDATE geosiret SET nbemp=23 WHERE tefet='12';
UPDATE geosiret SET nbemp=58 WHERE tefet='21';
UPDATE geosiret SET nbemp=110 WHERE tefet='22';
UPDATE geosiret SET nbemp=220 WHERE tefet='31';
UPDATE geosiret SET nbemp=280 WHERE tefet='32';
UPDATE geosiret SET nbemp=550 WHERE tefet='41';
UPDATE geosiret SET nbemp=1100 WHERE tefet='42';
UPDATE geosiret SET nbemp=2200 WHERE tefet='51';
UPDATE geosiret SET nbemp=5300 WHERE tefet='52';

ALTER TABLE geosiret ADD COLUMN geom geometry(Point, 4326);
UPDATE geosiret SET geom = ST_SetSRID(ST_MakePoint(lon, lat), 4326);
ALTER TABLE geosiret
ADD COLUMN maillegid INTEGER;
UPDATE geosiret SET maillegid=gid from SELECT mailles.gid as gid FROM geosiret,mailles  where st_within(geosiret.geom,ST_makeValid(mailles.geom)));
```
On a ajouté une colonne maillegid permettant de retrouver dans quelle maille du réseau de voirie est situé l'établissement.


On arrive à 69000 etablissements avec des effectifs (Et 969000 pour lequels emploi = 0 ! : on supprime ces entrées)
`DELETE FROM geosiret where nbemp=0;`
Il en reste 69297, dont 503 sans géom (ni lat/ ni lon), on les supprime, tant pis.
`DELETE FROM public.geosiret where lon is null or lat is null;`
