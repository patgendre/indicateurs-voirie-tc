-- fonction pl/sql créant une table de tronçons d'intersection à intersection (out line geom)
--                à partir d'une table de troncons et d'une table de points d'intersection entre ces tronçons
--    (les intersections sont préalablement calculées à partir du script SQL ci-dessus pour une table d'entrée OSM)
-- la nouvelle table de tronçons permet ensuite facilement de créer une table d'arcs pour un graphe routable 
--        alimentant par exemple pgrouting ('ways')
-- dernière mise  jour p Gendre CETE Med 07/06/10
-- source: http://postgis.refractions.net/pipermail/postgis-users/2006-March/011392.html
--   et    http://postgis.refractions.net/pipermail/postgis-users/2007-September/017159.html
-- màj 2019 : les liens ne pointent plus sur les bons messages

CREATE OR REPLACE FUNCTION public.__split_lines2(IN lineq text,IN pointq text,OUT lineid integer,OUT segid text,OUT line geometry)
    RETURNS SETOF record
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100    ROWS 1000 
AS $BODY$
DECLARE
	linerec record;
	pointrec record;
	linepos float;
	start_ float;
	end_ float;
	loopqry text;
	i int;

BEGIN
raise notice 'on commence';
 EXECUTE 'CREATE TEMP TABLE line_tmp as '|| lineq;
 EXECUTE 'CREATE TEMP TABLE point_tmp as '|| pointq;
 FOR linerec in EXECUTE 'select * from line_tmp order by id' LOOP
 start_ := 0;
-- loopqry := 'SELECT *,
-- ST_line_locate_point(ST_GeomFromText('||quote_literal(ST_AsText(linerec.geom))||',4326),point_tmp.geom) as frac from
-- point_tmp where ST_intersects(point_tmp.geom,ST_GeomFromText('||quote_literal(ST_AsText(linerec.geom))||
--  ',4326)) ORDER BY ST_line_locate_point(ST_GeomFromText('||quote_literal(ST_AsText(linerec.geom))||',4326),point_tmp.geom)';

-- RAISE NOTICE 'query=%',loopqry;
   
i := 0;
FOR pointrec IN SELECT *,ST_LineLocatePoint(linerec.geom, point_tmp.geom) as
   frac from point_tmp where ST_intersects(point_tmp.geom,linerec.geom) ORDER BY
   ST_line_locate_point(linerec.geom, point_tmp.geom) LOOP
--  FOR pointrec in EXECUTE loopqry LOOP
  i := i+1;
  end_ := pointrec.frac;
  lineid := linerec.id;
  RAISE NOTICE 'start=%,end=%',start_, end_;
  line := ST_LineSubstring(linerec.geom, start_, end_);
  segid := 'N' || lineid || i;
-- segid est sensé fournir une clé (unique) par concaténation
  start_:= end_;
  RETURN NEXT;
 END LOOP;
 line:= ST_LineSubstring(linerec.geom, end_,1.0);
 RETURN NEXT;
 END LOOP;
 DROP TABLE line_tmp;
 DROP TABLE point_tmp;
 RETURN;
END;

$BODY$;
LANGUAGE 'plpgsql';

-- exemple d'utilisation 
-- exemple create table seg as select * from split_lines2('select id as id, linestring as geom from voirie','select inter_geom as geom, gid as pid from intersections');
-- DELETE FROM seg WHERE GeometryType(line) != 'LINESTRING' ;
