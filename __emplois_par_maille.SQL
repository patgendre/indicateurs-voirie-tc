CREATE OR REPLACE FUNCTION public.__emplois_par_maille()
    RETURNS text
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100
AS $BODY$
-- calcul le nb d'emploi par mailles
-- suppose qu'existent la table mailles et la table geosiret issue des données SIRENE

declare
rec_maille RECORD;
rec_et RECORD;
current_index integer;
somme real;
rapport real;
intersection_area real;
carreau_surface real;
begin
current_index = 0;
somme=0;
FOR rec_maille IN select geom,gid from mailles
LOOP
	somme=0;
	raise notice 'gid %', rec_maille.gid;
	for rec_et in ( select nbemp,geom from geosiret where st_within(geom,ST_CollectionExtract(ST_makeValid(rec_maille.geom),3)))
	-- see https://gis.stackexchange.com/questions/283362/postgis-error-relate-operation-called-with-a-lwgeomcollection-type
	Loop																				  
		somme = somme + rec_et.nbemp;
	    raise notice 'somme %',somme;
	End Loop;
	update mailles set emplois=somme where gid=rec_maille.gid;
	somme=0;
	current_index = current_index+1;
	if current_index % 10 = 0
		THEN RAISE NOTICE 'Indice %', current_index || ' date =' || current_date;
	end if;
END LOOP;
RETURN 'Fini' ;
end;$BODY$;